#include <time.h> //Se usa al generar numeros aleatorios
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h> //Para validar entradas
/*
*Strings literales que son los nombres de las ciudades
*/
char* s1 = "Alajuelita";
char* s2 = "Barcelona";
char* s3 = "Birmingham";
char* s4 = "Bucharest";
char* s5 = "Desamparados";
char* s6 = "Dubai";
char* s7 = "Hatillos";
char* s8 = "Tokyo";
char* s9 = "Santiago";
char* s10 = "Shangai";

/*
*Todos los structs utilizados
*/
struct tabla{
    struct derivado** array;
};

struct derivado{
    char * nombre;
    int id; //Empieza en 0
    int modo_intercambio; //Si es recepcion un 1, si no un 0
    int valor_intercambio;
    int cantidad;
};


struct mis_derivados{
    int dinero;
    struct derivado** mitabla;
};

struct ciudad{
    char* nombre;
    int interes;
    int quiere_intercambio; //Si el interes es mayor o igual a 30, es un 1
    struct tabla* tablapropia;
};

struct nodo{
    int numero_nodo; //Empieza en 1
    struct ciudad* mi_ciudad;
    struct nodo* sgte;
};
struct lista{
    struct nodo* inicio;
};
/*
*Funciones y metodos utilizados.
*/

/*
*Crea un derivado para la tabla de dispersion
Los derivados ID de los derivados empiezan en 0
*/
struct derivado* crear_derivado(char* nombrederivado, int id){ 
    struct derivado* nvo_derivado = calloc(1,sizeof(struct derivado));
    nvo_derivado->nombre = nombrederivado;
    nvo_derivado->id = id;
    nvo_derivado->modo_intercambio = rand()%2;
    nvo_derivado->cantidad = rand()%5+1;
    nvo_derivado->valor_intercambio= rand()%100+1;
    return nvo_derivado;
}
/*
*Funcion de crear la tabla de derivados del usuario y su dinero inicial
*/
struct mis_derivados* generar_mi_tabla(char nombres[][13]){
    struct mis_derivados* mis_cosas = calloc(1,sizeof(struct mis_derivados));
    mis_cosas->dinero = 100; //Que el usuario inicie con 100 rupias
    struct derivado** tablita = calloc(6,sizeof(struct derivado));
    for (int i =0;i<6;i++){ //Crear unos derivados al usuario
        struct derivado* nvo_derivado = (struct derivado*)crear_derivado(nombres[i],i);
        tablita[i] = nvo_derivado;
    }
    mis_cosas->mitabla = tablita;
    return mis_cosas;
}
/*
*Funcion de imprimir un derivado del usuario
*/
void imprimir_derivado_usuario(struct derivado* d){
    printf("Derivado numero: %d - ",d->id);
    printf("%s - ",d->nombre);
    printf("Cantidad actual: %d. \n",d->cantidad);
}
/*
*Funcion de imprimir el dinero y derivados del usuario
*/
void imprimir_usuario(struct mis_derivados* mis_cosas){
    printf("  \n");
    printf("Su cantidad de dinero actual es: %d",mis_cosas->dinero);
    struct derivado** tablita = mis_cosas->mitabla;
    for (int i=0;i<6;i++){
        imprimir_derivado_usuario(tablita[i]);
    }
    printf("  \n");
}

/*
*Funcion de hash utilizada para la tabla de dispersion
*/
int hash(int i){
    return ((i*7)+3)%17;
}
/*
*Crea una tabla de dispersion
*/
struct tabla* crear_tabla_dispersa(char nombres[][13]){
    struct tabla* nva_tabla= calloc(1,sizeof(struct tabla));
    struct derivado** arreglo = calloc(17,sizeof(struct derivado)); //***********************
    for (int i=0;i<6;i++){
        int mihash = hash(i);
        arreglo[mihash] = crear_derivado(nombres[i],i);
    }
    nva_tabla->array = arreglo;
    return nva_tabla;
}
/*
*Funcion de intercambio 
*/
void intercambio (struct ciudad* c, struct mis_derivados* usuario, int id_producto, int cantidad,int modo){
    struct derivado** arreglo = c->tablapropia->array;
    int mihash = hash(id_producto);
    struct derivado** tabla_derivados_usuario = usuario->mitabla;
    struct derivado* derivado_usuario =tabla_derivados_usuario[id_producto];
    if (modo==1){//La ciudad compra, el usuario vende
        arreglo[mihash]->cantidad += cantidad; //Sumarle la cantidad a la ciudad
        derivado_usuario->cantidad -= cantidad; //Quitarle la cantidad al usuario
        usuario->dinero += (arreglo[mihash]->valor_intercambio)*cantidad; //Anadir dinero al usuario
    }
    else{ //La ciudad vende, el usuario compra
        arreglo[mihash]->cantidad -= cantidad; //Quitarle la cantidad a la ciudad
        derivado_usuario->cantidad += cantidad; //Sumarle la cantidad al usuario
        usuario->dinero -= (arreglo[mihash]->valor_intercambio)*cantidad; //Quitar dinero al usuario
    }
}
/*
*Borrar la tabla de una ciudad
*/
void borrar_tabla_ciudad(struct ciudad* c){
    struct tabla* tablilla = c->tablapropia;
    struct derivado** arreglo = tablilla->array;
    for (int i =0;i<=16;i++){ //for que borra los derivados
        free(arreglo[i]);
        arreglo[i]=0;
    }
    free(c->tablapropia->array);
    c->tablapropia->array=NULL;
    free(c->tablapropia);
    c->tablapropia = NULL;
    c->quiere_intercambio=0;
}
/*
*Funcion de imprimir un derivado de la tabla de una ciudad
*/
void imprimir_derivado_tabla(struct derivado* d){
    printf("Derivado %d - ",d->id);
    printf("%s. \n",d->nombre);
    if (d->modo_intercambio==1){ //Si el modo es recepcion
        printf("-Esta ciudad solamente puede comprar %d como maximo, por un precio de %d por unidad.\n",d->cantidad,d->valor_intercambio);
    }
    else{//Si la ciudad vende derivados
        printf("-Esta ciudad solo vende %d en total, por un  precio de %d por unidad.\n",d->cantidad,d->valor_intercambio);
    }
}
/*
*
*/
void imprimir_tabla_dispersa(struct tabla* t){
    struct derivado ** arreglo = t->array;
    for (int i=0;i<6;i++){
        int mihash = hash(i);
        imprimir_derivado_tabla(arreglo[mihash]);
    }
}
/*
*Crea una lista de nodos que contienen ciudades
*/
struct lista* crea_lista(){
    struct lista* listanueva = (struct lista*)calloc(1,sizeof(struct lista)); //Pide memoria para una lista nueva y devuelve el puntero a esa memoria
    return listanueva;
}
/*
*Verificar si un entero esta en un arreglo de enteros
*Si no esta, devuelve 0, si esta, devuelve 1
*/
int esta_en_arreglo(int* arreglo,int tamano, int numero){
    for (int i=0;i<tamano;i++){
        if (arreglo[i] == numero){
            return 1;
        }
    }
    return 0;
}
/*
*Llenar de -1 un arreglo de enteros
*/
void llena_negativos(int* arreglo,int tamano){
    for(int i=0;i<tamano;i++){
        arreglo[i]=-1;
    }
}
/*
*Crea un arreglo de un tamano x de enteros aleatorios desde 0 hasta un tamano maximo z
*/
int* crea_arreglo_int_random(int tamano,int maximo){
    int* arreglo_respuesta = calloc(tamano,sizeof(int)); //Pide memoria para el nuevo arreglo
    llena_negativos(arreglo_respuesta,tamano); //Pone -1 en el arreglo
    for(int i=0;i<tamano;i++){ 
        int num_aleatorio = rand() % maximo;
        while (esta_en_arreglo(arreglo_respuesta,tamano,num_aleatorio) == 1){ //Para no anadir repetidos
            num_aleatorio = rand() % maximo;
        }
        arreglo_respuesta[i] = num_aleatorio;
    }
    return arreglo_respuesta;
}
/*
*Crea una nueva ciudad, recibe un arreglo de ciudades y una posicion para encontrar su nombre
*/
struct ciudad* crear_ciudad(char** arreglo_ciudades,int posicion,char nombres_der[][13]){
    struct ciudad* nva_ciudad = calloc(1,sizeof(struct ciudad));
    nva_ciudad->nombre = arreglo_ciudades[posicion];
    nva_ciudad->interes = rand()%100;
    if (nva_ciudad->interes >= 30){
        nva_ciudad->tablapropia = crear_tabla_dispersa(nombres_der);
        nva_ciudad->quiere_intercambio =1;
    }
    else{
        nva_ciudad->quiere_intercambio =0;
    }
    return nva_ciudad; 
}
/*
*Imprime un nodo de la lista de ciudades
*/
void imprimir_nodo(struct nodo* n){
    struct ciudad* c = n->mi_ciudad;
    printf("+Ciudad %d, %s \n",n->numero_nodo,c->nombre);
    printf("Porcentaje de interes: %d%%. ",c->interes);
    if (c->quiere_intercambio==0){
        printf("No esta interesada en realizar intercambios. \n");
    }
    else{
        printf("Esta interesada en realizar intercambios. \n");
        imprimir_tabla_dispersa(c->tablapropia);
    }
}
/*
*Imprime todos los nodos de la lista de ciudades
*/
void imprimir_lista(struct lista* lista){
    printf("Se tienen las siguientes ciudades:\n\n");
    struct nodo* act = lista->inicio;
    while (act->sgte != NULL){ //Ir hasta el penultimo nodo
        imprimir_nodo(act);
        act = act->sgte;
        printf(" \n\n");
    }
    imprimir_nodo(act); //Imprimir el ultimo nodo
    printf(" \n\n");
    printf("%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%%-%% \n");
}
/*
*Incrementa el interes en un numero random entre 0 y 15
*/
void subir_interes(struct ciudad* ciudad, char nombres_derivados[][13]){
    ciudad->interes += (rand()%15) + 1;
    if (ciudad->interes >100){ //Si le sumo de mas
        ciudad->interes-= ciudad->interes%100;
    }
    if (ciudad->interes >= 30){
        ciudad->quiere_intercambio=1;
        if (ciudad->tablapropia == NULL){
            struct tabla* tabla_nva = crear_tabla_dispersa(nombres_derivados);
            ciudad->tablapropia = tabla_nva;
        }
    }
}
/*
*Disminuye el interes en un numero random entre 0 y la mitad del maximo de interes de una ciudad.
Si el interes de la ciudad es 0. No le resta nada.
Si el interes baja de 30, borra la tabla de dispersion y el estado de querer intercambiar.
*/
void bajar_interes(struct ciudad* ciudad){
    int maximo = ciudad->interes;
    maximo/=2;
    if (maximo==1){
        ciudad->interes = 0; //Si el interes es 1, restarle 1
    }
    if (maximo!=0 && maximo>1){
        ciudad->interes -= (rand()%maximo) + 1;
    }
    if (ciudad->interes<30){
        ciudad->quiere_intercambio = 0;
        if(ciudad->tablapropia != NULL){ //Si no se ha borrado la tabla de dispersion, borrarla
            borrar_tabla_ciudad(ciudad);
        }
    }
}
/*
*Baja interes a todas las ciudades excepto a una ciudad especificada por un numero a la cual le sube el interes
*Se pasa los nombres de derivados en caso de que al subir un interes, se necesite generar una tabla de dispersion nueva
*/
void cambiar_intereses_positivo(struct lista* lista, int numero, char nombres_derivados[][13]){
    struct nodo* act = lista->inicio;
    while (act->sgte !=NULL){ //Recorre, pero no ejecuta lo del while al ultimo.
        if (act->numero_nodo != numero){ //Si no es el numero de ciudad que hay que subir, se le baja
            bajar_interes(act->mi_ciudad);
        }
        else{ //Si es la ciudad a la que hay que aumentarle, se le aumenta
            subir_interes(act->mi_ciudad,nombres_derivados);
        }
        act = act->sgte;
    }
    if (act->numero_nodo != numero){
        bajar_interes(act->mi_ciudad);
    }
    else{
        subir_interes(act->mi_ciudad,nombres_derivados);
    }
}
/*
*Baja interes a todas las ciudades
*/
void bajar_todos_intereses(struct lista* lista){
    struct nodo* act = lista->inicio;
    while (act->sgte !=NULL){ //Recorre hasta el penultimo nodo y baja intereses
        bajar_interes(act->mi_ciudad);
        act=act->sgte;
    bajar_interes(act->mi_ciudad); //Baja interes del ultimo nodo.
    }
}
/*
*Crea las ciudades al inicio del juego y devuelve una lista de ciudades. La lista consiste en nodos con
una ciudad, el numero de ciudad y un puntero al siguiente nodo.
*/
struct lista* crear_ciudades(char** arreglo_ciudades, int num_ciudades,char nombres_der[][13]){
    int* arreglo_ints = crea_arreglo_int_random(num_ciudades,10); //Arreglo con ints aleatorios no repetidos    
    struct lista* lista_respuesta = crea_lista(); //Crear el struct de la lista
    int num_ciudad = 0; //Variable que va a servir en el for para encontrar el nombre de la ciudad
    struct nodo* nodo_anterior; //Variable que va a servir en el for
    
    //For que crea la lista, un nodo va a tener una ciudad, un numero y un puntero
    for(int i =0;i<num_ciudades;i++){
        num_ciudad = arreglo_ints[i]; //Posicion para encontrar el nombre de la ciudad en arreglo_ciudades
        struct ciudad* nva_ciudad = crear_ciudad(arreglo_ciudades,num_ciudad,nombres_der);
        struct nodo* nvo_nodo = calloc(1,sizeof(struct nodo)); //Crear nuevo nodo
        nvo_nodo->numero_nodo = i+1; //Asignarle el numero
        nvo_nodo->mi_ciudad = nva_ciudad; //Asignarle el struct de la ciudad creada
        if (i>0){
            nodo_anterior->sgte = nvo_nodo; //Que el nodo anterior apunte al nuevo
        }
        else{ // i = 0
            lista_respuesta->inicio = nvo_nodo; //Que lista apunte al primer nodo
        }
        nodo_anterior = nvo_nodo;
    }
    return lista_respuesta;
}

void borrar_contenido_nodo(struct nodo* n){
    n->numero_nodo = 0;
    struct ciudad* c = n->mi_ciudad;
    c->quiere_intercambio = 0;
    c->interes = 0;
    c->nombre = 0;
    if(n->mi_ciudad->tablapropia != NULL){
        borrar_tabla_ciudad(n->mi_ciudad);
    }
}

void borrarlista(struct lista* lista){
    struct nodo* act = lista->inicio;
    while (act->sgte !=NULL){
        struct nodo* copia = act;
        act=act->sgte;
        borrar_contenido_nodo(copia);
        free(copia);
        copia = NULL;
    }
    borrar_contenido_nodo(act);
    free(act);
    act=NULL;
}

/*
*Ver el largo de una lista
*/
int ver_largo(struct lista* l){
    int resp =0;
    struct nodo* act = l->inicio;
    while (act->sgte != NULL){ //Recorrer hasta el penultimo nodo
        resp+=1;
        act = act->sgte;
    }
    resp+=1;
    return resp;
}
/*
*Verificar que se pueda realizar un intercambio
*/
int hay_intercambio(struct lista* l){
    int resp = 0;
    struct nodo* act = l->inicio;
    while (act->sgte != NULL){ //deja a act en el ultimo nodo, sin embargo, no aplica lo del while al ultimo nodo
        if (act->mi_ciudad->quiere_intercambio == 1){
            return 1;
        }
        act = act->sgte;
    }
    if(act->mi_ciudad->quiere_intercambio == 1){ //Fijarse si la ultima ciudad quiere un intercambip
        return 1;
    }
    return 0;
}
/*
*Verifica si se puede hacer intercambio en una ciudad, conociendo su id. Retorna 1 si la ciudad puede intercambiar, 0 si no.
*/
int hay_intercambio_ciudad(struct lista* l, int id){
    int resp = 0;
    struct nodo* act = l->inicio;
    while (act->sgte != NULL){ //deja a act en el ultimo nodo, sin embargo, no aplica lo del while al ultimo nodo
        if (act->numero_nodo == id){
            if(act->mi_ciudad->quiere_intercambio ==1){
                return 1;
            }
            else{
                return 0;
            }
        }
        act = act->sgte;
    }
    if(act->mi_ciudad->quiere_intercambio ==1){
        return 1;
    }
    else{
        return 0;
    }
}
/*
*Metodos y funciones del menu.
*/
/*
*Funcion de crear arreglo, que genera un arreglo de strings que hace mas comodo acceder a las ciudades
*/
char** crea_arreglo_ciudades(){
    char ** arreglo = calloc(10,sizeof(char**));
    arreglo[0] = s1;
    arreglo[1] = s2;
    arreglo[2] = s3;
    arreglo[3] = s4;
    arreglo[4] = s5;
    arreglo[5] = s6;
    arreglo[6] = s7;
    arreglo[7] = s8;
    arreglo[8] = s9;
    arreglo[9] = s10;
    return arreglo;
}
/*
*Funcion que lee un entero que se encuentre entre un rango
*/
int leer_entero(int num_min, int num_max){
    int resp = -1;
    scanf("%d",&resp);
    while (resp<num_min || resp>num_max){
        printf("Numero invalido, pruebe de nuevo. \n");
        scanf("%d",&resp);
    }
    return resp;
}
/*
*Crea y retorna una lista con las ciudades escogidas para el juego
*/
struct lista* empezar_juego(char nombres_der[][13]){
    printf("Bienvenido. El objetivo de este juego es lograr conseguir 1500 rupias en 15 turnos. \n");
    char ** todas_ciudades = crea_arreglo_ciudades(); //Array de strings para los nombres de las ciudades. 
    printf("Digite un cuantas ciudades desea utilizar (numero del 3 al 10): ");
    int cant_ciudades = leer_entero(3,10);
    struct lista* lista = crear_ciudades(todas_ciudades,cant_ciudades,nombres_der); //Crea la lista de ciudades
    free(todas_ciudades); //Ya no se va a utilizar mas.
    return lista;
}
/*
*Ver si se quiere subir un interes o realizar un intercambio
Si es subir interes, devuelve 0
Si es intercambiar, devuelve 1
*/
int accion_1(struct lista* l){
    printf("********************************** \n");
    int intercambio = hay_intercambio(l);
    if (intercambio==1){ //Si es posible realizar un intercambio.
        printf("Para incrementar el interes de una ciudad, digite 0. \n");
        printf("Para realizar un intercambio de una ciudad, digite 1. \n");
        int primera_accion = leer_entero(0,1);
        return primera_accion;  
    }
    else{
        printf("No es posible realizar un intercambio, se procedera a incrementar el interes de alguna ciudad. \n");
        return 0;
    }
}

void accion_2_subir(struct lista* l,char nombres_derivados[][13]){
    int largo = ver_largo(l);
    printf("Digite el numero de la ciudad a la que desea subir interes. \n");
    int num_ciudad = leer_entero(1,largo); //Se pone largo y no largo -1 porque los nodos de ciudades empiezan en 1, no en 0.
    cambiar_intereses_positivo(l,num_ciudad,nombres_derivados);
    printf("Interes aumentado. \n");
    printf("********************************** \n\n");
}
/*
*Valida que la entrada del usuario sea una ciudad que se pueda intercambiar, devuelve la ciudad
*/
struct ciudad* accion_3_num_ciudad_intercambio(struct lista* l){
    printf("Ingrese el numero de la ciudad con la cual desea intercambiar. \n");
    int ingreso = leer_entero(1,ver_largo(l));
    while (hay_intercambio_ciudad(l,ingreso) != 1){
        printf("Esta ciudad no tiene interes en realizar intercambios. Pruebe de nuevo. \n");
        ingreso = leer_entero(1,ver_largo(l));
    }
    //Aqui el numero ingresado es correcto, se procede a devolver el puntero de la ciudad
    struct nodo* act = l->inicio;
    while (act->numero_nodo != ingreso){
        act=act->sgte;
    }
    return act->mi_ciudad;
}
/*
*Metodo que hace el intercambio, recibe la ciudad a la que se hace el intercambio y el deposito del usuario
*/
void accion_4_intercambiar(struct ciudad* c, struct mis_derivados* deposito){
    printf("Ingrese el numero del producto que desea comprar. (ver los derivados de la ciudad) \n");
    int num_prod = leer_entero(0,5);
    int prod_en_array = hash(num_prod);
    struct derivado* derivado_ciudad = c->tablapropia->array[prod_en_array];
    if (derivado_ciudad->modo_intercambio == 1){                                //La ciudad compra, el usuario vende
        printf("Ingrese la cantidad que desea vender a esta ciudad. \n");
        int cant_prod = leer_entero(0,derivado_ciudad->cantidad);              //Lee la entrada
        while (cant_prod > deposito->mitabla[num_prod]->cantidad){ //Si se intenta vender mas productos que no tengo.
            printf("Usted no tiene tal cantidad de producto(s), pruebe de nuevo. (Si no tiene, escriba 0)\n");
            cant_prod = leer_entero(0,derivado_ciudad->cantidad);
        }
        derivado_ciudad->cantidad += cant_prod;
        deposito->dinero += cant_prod*(derivado_ciudad->valor_intercambio);
        deposito->mitabla[num_prod]->cantidad -= cant_prod;
        printf("Usted ha vendido (%d) %s",cant_prod,derivado_ciudad->nombre);
    }
    else{                                                        //La ciudad vende, el usuario compra.
        printf("Ingrese la cantidad que desea comprar: \n");
        int cant_prod = leer_entero(0,derivado_ciudad->cantidad);
        int costo = cant_prod*derivado_ciudad->valor_intercambio;
        while (costo > deposito->dinero){                            //Que el usuario no pueda comprar tal cantidad
            printf("Usted no puede comprar tal cantidad, pruebe de nuevo. (Si no puede comprar ni uno solo, escriba 0)\n");
            cant_prod = leer_entero(0,derivado_ciudad->cantidad);
            costo = cant_prod*derivado_ciudad->valor_intercambio;
        }
        //Aqui el usuario ingreso la cantidad correcta que puede comprar.
        derivado_ciudad->cantidad -= cant_prod;
        deposito->dinero-= costo;
        deposito->mitabla[num_prod]->cantidad += cant_prod;
        printf("Usted ha comprado (%d) %s \n",cant_prod,derivado_ciudad->nombre);
    }
}
/*
*Funcion que imprime al usuario sus derivados y su dinero
*/
void imprimir_deposito(struct mis_derivados* m){
    printf("Su dinero actual es de %d rupias. \n",m->dinero);
    printf("Usted tiene la siguiente cantidad de derivados.\n");
    for (int i =0;i<6;i++){
        struct derivado* der = m->mitabla[i];
        printf("%s: %d \n",der->nombre,der->cantidad);
    }
}

int main(){
    srand(time(NULL)); //Inicializar funcion de aleatorio, de no hacerlo repite los mismos numeros al correr el programa de nuevo
    char nombres_derivados[][13] = {"Papel","Cuerdas","Combustible","Pintura","Costemicos","Semillas"}; //Nombres de lso porductos
    struct mis_derivados* deposito = generar_mi_tabla(nombres_derivados);
    struct lista* lista = empezar_juego(nombres_derivados); //Crea y retorna la lista de ciudades
    int turnos = 0;
    int dinero = deposito->dinero;
    while (turnos<15 && dinero<1500){
        imprimir_lista(lista);
        imprimir_deposito(deposito);
        int prim_accion = accion_1(lista);
        if (prim_accion == 0){ //Si se desea subir el interes de una ciudad
            accion_2_subir(lista,nombres_derivados);
        }
        else{ //Si se desea realizar un intercambio
            struct ciudad* ciudad_intercambio = accion_3_num_ciudad_intercambio(lista);
            accion_4_intercambiar(ciudad_intercambio,deposito);
            bajar_todos_intereses(lista);
        }
        turnos+=1;
    }
    if (dinero>=1500){
        printf("Usted ha ganado. \n");
    }
    else{
        printf("Usted ha perdido. \n");
    }
    borrarlista(lista);
return 0;
}
